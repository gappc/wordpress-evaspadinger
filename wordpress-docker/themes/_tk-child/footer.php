<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the id=main div and all content after
 *
 * @package _tk
 */
?>
			</div><!-- close .*-inner (main-content or sidebar, depending if sidebar is used) -->
		</div><!-- close .row -->
	</div><!-- close .container -->
</div><!-- close .main-content -->

<footer id="colophon" class="site-footer" role="contentinfo">
<?php // substitute the class "container-fluid" below if you want a wider content area ?>
	<div class="container">
		<div class="row">
			<div class="footer-contact col-sm-4">
				<h5>Kontakt</h5>
				<?php print_contact(); ?>
			</div>
			<div class="footer-center col-sm-4">
				<img src="<?php echo esc_url( get_theme_mod( 'es_footer_logo' ) ); ?>" alt="Ebenenwechsel Logo" />
			</div>
			<div class="footer-links col-sm-4">
				<h5>Quicklinks</h5>
				<div>
					<a href="<?php echo get_permalink( get_theme_mod( 'es_footer_partner' ) ) ?>" title="Partner">Partner</a>
				</div>
				<?php
				for($i = 1; $i < 6; $i++) {
					$link = get_theme_mod( 'es_footer_ql' . $i . '_link' );
					$text = get_theme_mod( 'es_footer_ql' . $i . '_text' );
					if (isset($link) and isset($text)) {
						echo "<div><a href='" . $link . "'>" . $text . "</a></div>";
					}
				}
				?>
			</div>
		</div>
		<div class="row">
			<div class="footer-legal col-sm-12">
				<div><a href="/impressum" alt="Impressum">Impressum</a> | <a href="/privacy" alt="Privacy">Privacy</a></div>
			</div>
		</div>
	</div><!-- close .container -->
</footer><!-- close #colophon -->

<?php wp_footer(); ?>

</body>
</html>
