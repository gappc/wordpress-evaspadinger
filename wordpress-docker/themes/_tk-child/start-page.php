<?php
/*
 * Template Name: Start Seite
 */

get_header(); ?>

<div class="col-sm-8">
<?php while ( have_posts() ) : the_post(); ?>

	<?php get_template_part( 'content', 'page' ); ?>

<?php endwhile; // end of the loop. ?>
</div>

<div class="es-main col-sm-4">
	<div class="es-termine">
		<h3>Termine</h3>
		<?php
			$raw_dates = get_theme_mod( 'es_termine_datepicker' );
			if ( $raw_dates and strlen( $raw_dates ) > 0 ) {
				$dates = explode( ",", $raw_dates);
				foreach ($dates as $date) {
					if (time() - strtotime($date) < 0) {
						echo "<span class='es-termin-future'>" . $date . "</span> ";
					}
					else {
						echo "<span class='es-termin-past'>" . $date . "</span> ";
					}
				}
			}
		?>
	</div>
	<?php if ( get_theme_mod( 'es_info_folder_file' ) ) { ?>
		<div class="es-info-folder">
			<h4><a href="<?php echo esc_url( get_theme_mod( 'es_info_folder_file' ) ); ?>" alt="<?php echo get_theme_mod( 'es_info_folder_text' ); ?>"><?php echo get_theme_mod( 'es_info_folder_text' ); ?></a></h4>
		</div>
	<?php } ?>
	<?php echo es_get_posts( "es_print_start_aktuelles" ) ?>
</div>


<?php get_footer(); ?>
