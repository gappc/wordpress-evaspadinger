<?php
/*
 * Template Name: Start Seite 3 Spalten
 */

get_header(); ?>

<div class="col-sm-4">
<?php while ( have_posts() ) : the_post(); ?>

	<?php get_template_part( 'content', 'page' ); ?>

<?php endwhile; // end of the loop. ?>
</div>

<div class="es-main col-sm-4">
	<div class="es-termine">
		<h3>Termine</h3>
		<article class="panel panel-default panel-body">
      <header>
      <h4><a href="http://localhost:11080/2015/10/ausbildungslehrgang-asosc-2016-innsbruck/">Systemaufstellungen in der Gruppe</a></h4>
      <h5>für Klienten, lernende und Interessierete</h5>
    	</header>
  		<span class="es-termin-past">01.10.2015</span> <span class="es-termin-past">09.10.2015</span> <span class="es-termin-past">21.10.2015</span> <span class="es-termin-past">22.10.2015</span> <span class="es-termin-future">30.10.2015</span>
			<h4 style="margin-top:30px"><a href="http://localhost:11080/2015/10/ausbildungslehrgang-asosc-2016-innsbruck/">Systemaufstellungen im Einzelcoaching</a></h4>
			<h5>auf Anfrage</h5>
  	</article>
	</div>
	<div class="es-bildungsangebote">
		<h3>Bildungsangebote</h3>
		<article class="panel panel-default panel-body">
			<ul class="fa-ul">
				<li><i class="fa-li fa fa-check"></i><a href=".">Ausbildungslehrgang Salzburg 2016</a></li>
				<li><i class="fa-li fa fa-check"></i><a href=".">Ausbildungslehrgang Innsbruck 2016</a></li>
				<li><i class="fa-li fa fa-check"></i><a href=".">Seminar Wifi Innsbruck</a></li>
				<li><i class="fa-li fa fa-check"></i><a href=".">Organisation Nals</a></li>
				<li><i class="fa-li fa fa-check"></i><a href=".">Ausbildungslehrgang Salzburg 2016</a></li>
			</ul>
		</article>
	</div>
</div>

<div class="es-main col-sm-4">
	<div class="es-termine">
		<h3>Aktuelles</h3>
	</div>
	<?php if ( get_theme_mod( 'es_info_folder_file' ) ) { ?>
		<div class="es-info-folder">
			<h4><a href="<?php echo esc_url( get_theme_mod( 'es_info_folder_file' ) ); ?>" alt="<?php echo get_theme_mod( 'es_info_folder_text' ); ?>"><?php echo get_theme_mod( 'es_info_folder_text' ); ?></a></h4>
		</div>
	<?php } ?>
	<?php echo es_get_posts( "es_print_start_aktuelles" ) ?>
</div>


<?php get_footer(); ?>
