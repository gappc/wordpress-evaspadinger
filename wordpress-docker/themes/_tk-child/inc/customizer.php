<?php

add_action( 'customize_register', 'es_customize_register' );
function es_customize_register( $wp_customize ) {
  $wp_customize->add_panel(
    'es_panel' ,
    array (
      'title' => __( 'Ebenenwechsel - Admin' ),
      'description' => 'Einstellungen der Webseite', // Include html tags such as <p>.
      'priority' => 1, // Mixed with top-level-section hierarchy.
    )
  );

  add_header_section( $wp_customize );
  add_footer_section( $wp_customize );
  add_contact_section( $wp_customize );
  add_info_folder_section( $wp_customize );
  add_termine_section( $wp_customize );
  add_aktuelles_section( $wp_customize );
  add_footer_section( $wp_customize );
}

function add_header_section( $wp_customize ) {
    $wp_customize->add_section(
      'es_header_section' ,
      array(
        'title'       => __( 'Kopfbereich' ),
        'description' => 'Einstellungen für den Kopfbereich der Webseite',
        'priority'    => 1,
        'panel'       => 'es_panel',
      )
    );

    $wp_customize->add_setting( 'es_header_foto' );
    $wp_customize->add_control(
      new WP_Customize_Image_Control(
        $wp_customize,
        'es_header_foto',
        array(
          'label'    => __( 'Foto Eva Spadinger' ),
          'section'  => 'es_header_section',
          'settings' => 'es_header_foto',
        )
      )
    );

    $wp_customize->add_setting( 'es_header_title' );
    $wp_customize->add_control(
      new WP_Customize_Control(
        $wp_customize,
        'es_header_title',
        array(
          'type'        => 'textarea',
          'section'     => 'es_header_section',
          'label'       => __( 'Zentrale Überschrift' ),
          'settings'    => 'es_header_title',
        )
      )
    );

    $wp_customize->add_setting( 'es_header_description' );
    $wp_customize->add_control(
      new WP_Customize_Control(
        $wp_customize,
        'es_header_description',
        array(
          'type'        => 'textarea',
          'section'     => 'es_header_section',
          'label'       => __( 'Zentrale Beschreibung' ),
          'settings'    => 'es_header_description',
        )
      )
    );

    $wp_customize->add_setting( 'es_header_logo' );
    $wp_customize->add_control(
      new WP_Customize_Image_Control(
        $wp_customize,
        'es_header_logo',
        array(
          'label'    => __( 'Logo' ),
          'section'  => 'es_header_section',
          'settings' => 'es_header_logo',
        )
      )
    );
}

function add_contact_section( $wp_customize ) {
  $wp_customize->add_section(
    'es_contact_section' ,
    array(
      'title'       => __( 'Kontaktdaten' ),
      'description' => 'Kontaktdaten ändern',
      'priority'    => 10,
      'panel'       => 'es_panel',
    )
  );

  $wp_customize->add_setting( 'es_contact_street' );
  $wp_customize->add_control(
    new WP_Customize_Control(
      $wp_customize,
      'es_contact_street',
      array(
        'type'        => 'text',
        'section'     => 'es_contact_section',
        'label'       => __( 'Strasse' ),
        'settings'    => 'es_contact_street',
      )
    )
  );

  $wp_customize->add_setting( 'es_contact_plz' );
  $wp_customize->add_control(
    new WP_Customize_Control(
      $wp_customize,
      'es_contact_plz',
      array(
        'type'        => 'text',
        'section'     => 'es_contact_section',
        'label'       => __( 'PLZ' ),
        'settings'    => 'es_contact_plz',
      )
    )
  );

  $wp_customize->add_setting( 'es_contact_phone' );
  $wp_customize->add_control(
    new WP_Customize_Control(
      $wp_customize,
      'es_contact_phone',
      array(
        'type'        => 'text',
        'section'     => 'es_contact_section',
        'label'       => __( 'Telefon' ),
        'settings'    => 'es_contact_phone',
      )
    )
  );

  $wp_customize->add_setting( 'es_contact_email' );
  $wp_customize->add_control(
    new WP_Customize_Control(
      $wp_customize,
      'es_contact_email',
      array(
        'type'        => 'email',
        'section'     => 'es_contact_section',
        'label'       => __( 'Email' ),
        'settings'    => 'es_contact_email',
      )
    )
  );

  $wp_customize->add_setting( 'es_contact_gmaps_lat' );
  $wp_customize->add_control(
    'es_contact_gmaps_lat',
    array(
      'type'        => 'text',
      'section'     => 'es_contact_section',
      'label'       => __( 'Google maps lat' ),
    )
  );

  $wp_customize->add_setting( 'es_contact_gmaps_lng' );
  $wp_customize->add_control(
    'es_contact_gmaps_lng',
    array(
      'type'        => 'text',
      'section'     => 'es_contact_section',
      'label'       => __( 'Google maps lng' ),
    )
  );

}

function add_info_folder_section( $wp_customize ) {
  $wp_customize->add_section(
    'es_info_folder_section' ,
    array(
      'title'       => __( 'Infofolder' ),
      'description' => 'Datei für den Infofolder auswählen',
      'priority'    => 20,
      'panel'       => 'es_panel',
    )
  );

  $wp_customize->add_setting( 'es_info_folder_file' );
  $wp_customize->add_control(
    new WP_Customize_Upload_Control(
      $wp_customize,
      'es_info_folder_file',
      array(
        'label'    => __( 'Infofolder' ),
        'section'  => 'es_info_folder_section',
        'settings' => 'es_info_folder_file',
      )
    )
  );

  $wp_customize->add_setting( 'es_info_folder_text' );
  $wp_customize->add_control(
    new WP_Customize_Control(
      $wp_customize,
      'es_info_folder_text',
      array(
        'type'        => 'text',
        'section'     => 'es_info_folder_section',
        'label'       => __( 'Beschriftung' ),
        'settings'    => 'es_info_folder_text',
      )
    )
  );
}

function add_termine_section( $wp_customize ) {
  $wp_customize->add_section(
    'es_termine_section' ,
    array(
      'title'       => __( 'Termine' ),
      'description' => 'Verwaltung der Termine',
      'priority'    => 30,
      'panel'       => 'es_panel',
    )
  );

  require_once dirname(__FILE__) . '/datepicker-custom-control.php';
  $wp_customize->add_setting( 'es_termine_datepicker' );
  $wp_customize->add_control(
    new Es_Customize_Datepicker_Control(
      $wp_customize,
      'es_termine_datepicker',
      array(
        'type'        => 'datepicker',
        'section'     => 'es_termine_section',
        'label'       => __( 'Datum für Systemaufstellung in der Gruppe' ),
        'settings'    => 'es_termine_datepicker',
      )
    )
  );

  $wp_customize->add_setting( 'es-termine-zusammenfassung' );
  $wp_customize->add_control(
    'es-termine-zusammenfassung',
    array(
      'label'    => __( 'Welche Seite wird im Abschnitt "Termine" auf der Startseite angezeigt' ),
      'section'  => 'es_termine_section',
      'type'     => 'dropdown-pages'
    )
  );

  require_once dirname(__FILE__) . '/post-sort-custom-control.php';
  $wp_customize->add_setting( 'es_termine_sort' );
  $wp_customize->add_control(
    new Es_Post_Sort_Control(
      $wp_customize,
      'es_termine_sort',
      array(
        'type'        => 'Termine',
        'section'     => 'es_termine_section',
        'label'       => __( 'Sortierung der Beiträge "Termine"' ),
        'settings'    => 'es_termine_sort',
      )
    )
  );
}

function add_aktuelles_section( $wp_customize ) {
  $wp_customize->add_section(
    'es_aktuelles_section' ,
    array(
      'title'       => __( 'Aktuelles' ),
      'description' => 'Verwaltung der Beiträge vom Typ "Aktuelles"',
      'priority'    => 40,
      'panel'       => 'es_panel',
    )
  );

  require_once dirname(__FILE__) . '/post-sort-custom-control.php';
  $wp_customize->add_setting( 'es_aktuelles_sort' );
  $wp_customize->add_control(
    new Es_Post_Sort_Control(
      $wp_customize,
      'es_aktuelles_sort',
      array(
        'type'        => 'Aktuelles',
        'section'     => 'es_aktuelles_section',
        'label'       => __( 'Sortierung der Beiträge "Aktuelles"' ),
        'settings'    => 'es_aktuelles_sort',
      )
    )
  );
}

function add_footer_section( $wp_customize ) {
  $wp_customize->add_section(
    'es_footer_section' ,
    array(
      'title'       => __( 'Fußbereich' ),
      'description' => 'Einstellungen für den Fußbereich der Webseite',
      'priority'    => 50,
      'panel'       => 'es_panel',
    )
  );

  $wp_customize->add_setting( 'es_footer_logo' );
  $wp_customize->add_control(
    new WP_Customize_Image_Control(
      $wp_customize,
      'es_footer_logo',
      array(
        'label'    => __( 'Logo' ),
        'section'  => 'es_footer_section',
        'settings' => 'es_footer_logo',
      )
    )
  );

  $wp_customize->add_setting( 'es_footer_partner' );
  $wp_customize->add_control(
    'es_footer_partner',
    array(
      'label'    => __( 'Verlinkte Seite für Partner' ),
      'section'  => 'es_footer_section',
      'type'     => 'dropdown-pages'
    )
  );

  for($i = 1; $i < 6; $i++) {
    $wp_customize->add_setting( 'es_footer_ql' . $i . '_link' );
    $wp_customize->add_control(
      'es_footer_ql' . $i . '_link',
      array(
        'type'        => 'text',
        'section'     => 'es_footer_section',
        'label'       => __( 'Quicklink ' . $i . ' - Link' ),
      )
    );

    $wp_customize->add_setting( 'es_footer_ql' . $i . '_text' );
    $wp_customize->add_control(
      'es_footer_ql' . $i . '_text',
      array(
        'type'        => 'text',
        'section'     => 'es_footer_section',
        'label'       => __( 'Quicklink ' . $i . ' - Text' ),
      )
    );
  }
}
