<?php

function insert_systemaufstellungen_gruppe_termine() {
	return get_systemaufstellungen_gruppe_termine();
}
add_shortcode( 'systemaufstellungen_gruppe_termine', 'insert_systemaufstellungen_gruppe_termine' );

function insert_adresse_strasse() {
  return get_theme_mod( 'es_contact_street' );
}
add_shortcode( 'adresse_strasse', 'insert_adresse_strasse' );

function insert_adresse_plz() {
  return get_theme_mod( 'es_contact_plz' );
}
add_shortcode( 'adresse_plz', 'insert_adresse_plz' );

function insert_adresse_telefon() {
  return get_theme_mod( 'es_contact_phone' );
}
add_shortcode( 'adresse_telefon', 'insert_adresse_telefon' );

function insert_adresse_email() {
  return get_theme_mod( 'es_contact_email' );
}
add_shortcode( 'adresse_email', 'insert_adresse_email' );

function insert_adresse_email_link() {
  $email = insert_adresse_email();
  return "<a title='Eva Spadinger Email' href='mailto:$email'>$email</a>";
}
add_shortcode( 'adresse_email_link', 'insert_adresse_email_link' );

?>
