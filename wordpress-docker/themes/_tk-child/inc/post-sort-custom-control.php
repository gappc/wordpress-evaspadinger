<?php

if (class_exists('WP_Customize_Control')) {
  class Es_Post_Sort_Control extends WP_Customize_Control {
      public $type = '';
      public $label = '';

      public function enqueue() {
        wp_enqueue_style( 'jquery-style', get_stylesheet_directory_uri() . '/inc/css/jquery-ui.min.css' );
        wp_enqueue_style( 'post-sort-style', get_stylesheet_directory_uri() . '/inc/css/post-sort-custom-control.css' );
        wp_enqueue_script( 'jquery-script', get_stylesheet_directory_uri() . '/inc/js/jquery-ui.min.js' );
      }

      public function render_content() {
        echo "type: $this->type";
        $args = array(
          'category_name'    => $this->type,
          'orderby'          => 'meta_value_num',
          'order'            => 'ASC',
          'meta_key'         => 'es-sortorder',
          'post_type'        => 'post',
          'post_status'      => 'publish',
          'suppress_filters' => true
        );
        $posts_array = get_posts( $args );

        if ($posts_array) { ?>
          <span class="customize-control-title"><?php echo esc_html( $this->label ); ?></span>
          <ul id="es-sortable">
            <?php foreach ( $posts_array as $post ) {
              $show = is_show_post( $post ) ? "checked" : "";
              ?>
              <li class="ui-state-default es-sortable-listitem" data-id="<?php echo $post->ID; ?>"><span class="ui-icon ui-icon-arrowthick-2-n-s"></span><input type="checkbox" name="show" <?php echo $show ?> /><?php echo $post->post_title ?></li>
            <?php } ?>
          </ul>
          <input id="es-sortable-input" <?php echo $this->get_link() ?> />
        <?php } ?>
        <script>
          (function($) {
            var sortableInput = $("#es-sortable-input");
            var sortable = $("#es-sortable");
            sortable.sortable({
              update: function(event, ui) {
                doUpdate();
              }
            });

            $("input", sortable).on("change", function() {
              doUpdate();
            });

            function doUpdate() {
              var data = [];
              var sortItems = $("li", sortable);
              $.each(sortItems, function(index, element) {
                var id = $(element).attr("data-id");
                var show = $("input", $(element)).prop('checked');
                data.push({ id : id, show: show });
              });
              //console.log(JSON.stringify(data));
              var jsonData = JSON.stringify(data)
              var postData = {
                'action': 'es_post_sort_action',
                'sortorder': jsonData,
                '_ajax_nonce': '<?php echo wp_create_nonce( 'evaspadinger' ); ?>'
              };
              $.post("admin-ajax.php", postData, function(response) {
                console.log('Got this from the server: ' + response);
                sortableInput.val(jsonData);
                sortableInput.keyup();
                $(".button.button-primary.save").click();
              });
            }
          })(jQuery);
        </script>
      <?php }
  }
}

?>
