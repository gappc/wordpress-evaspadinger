<?php

if (class_exists('WP_Customize_Control')) {
  class Es_Customize_Datepicker_Control extends WP_Customize_Control {
      public $type = 'datepicker';
      public $label = '';

      public function enqueue() {
        wp_enqueue_style( 'jquery-style', get_stylesheet_directory_uri() . '/inc/css/jquery-ui.min.css' );
        wp_enqueue_style( 'datepicker-style', get_stylesheet_directory_uri() . '/inc/css/jquery-ui.multidatespicker.css' );
        wp_enqueue_script( 'datepicker-script', get_stylesheet_directory_uri() . '/inc/js/jquery-ui.min.js' );
        wp_enqueue_script( 'multidatepicker-script', get_stylesheet_directory_uri() . '/inc/js/jquery-ui.multidatespicker.js' );
      }

      public function render_content() { ?>
            <span class="customize-control-title"><?php echo esc_html( $this->label ); ?></span>
            <div style="overflow:hidden;">
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-8">
                            <div id="datepicker"></div>
                            <input style="display:none;" id="datepicker-value" <?php echo $this->get_link() ?>/>
                        </div>
                    </div>
                </div>
                <script type="text/javascript">
                  (function($) {
                    var dates = loadDates("<?php echo $this->value() ?>");
                    console.log(dates);
                    try {
                      showDatePicker(dates);
                    } catch(e) {
                      alert("Leider gab es einen Fehler bei den Terminen. Um einen funktionierenden Zustand wiederherzustellen, werden alle Termine gelöscht und müssen noch einmal händisch angelegt werden");
                      showDatePicker();
                    }

                    function loadDates(rawData) {
                      if (rawData) {
                        var data = rawData.split(",").filter(function(a) {
                          var isOk = /([0-9]{2}\.[0-9]{2}\.[0-9]{4})$/.test(a);
                          if (isOk === true) {
                            return true;
                          }
                          else {
                            alert("Das Datum " + a + " konnte nicht wiederhergestellt werden. Bitte händisch erneut setzen");
                            return false;
                          }
                        });
                        return data;
                      }
                      else {
                        return [];
                      }
                    }

                    function showDatePicker(dates) {
                      var datePicker = $('#datepicker').multiDatesPicker({
                        dateFormat: "dd.mm.yy",
                        onSelect: function(se, ev) {
                          datepickerValue = $("#datepicker-value");
                          datepicker = $('#datepicker');
                          datepickerValue.val(datepicker.multiDatesPicker("getDates"));
                          datepickerValue.keyup();
                        }
                      });
                      if (dates !== 'undefined' && dates.length > 0) {
                        datePicker.multiDatesPicker("addDates", dates);
                      }
                    }
                  })(jQuery);
                </script>
            </div>
          <?php
      }
  }
}
