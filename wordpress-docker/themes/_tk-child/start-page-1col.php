<?php
/*
 * Template Name: Start Seite 1 Spalte
 */

get_header(); ?>

<div class="start-page col-sm-12">
<?php while ( have_posts() ) : the_post(); ?>

	<?php get_template_part( 'content', 'page' ); ?>

<?php endwhile; // end of the loop. ?>
</div>

<div class="es-main col-sm-6">
	<div class="es-termine">
		<h1>Termine</h1>
		<?php
			$args = array(
				'posts_per_page' => 1,
				'post_type' => 'page',
				'post__in' => array( get_theme_mod( 'es-termine-zusammenfassung' ) ),
			);
			$query = new WP_Query( $args );
			if ( $query->have_posts() ) : ?>
				<article class="panel panel-default panel-body">
					<?php while ( $query->have_posts() ) : $query->the_post();
						the_content();
					endwhile; ?>
					<?php edit_post_link( __( 'Edit', '_tk' ), '<footer class="entry-meta"><span class="edit-link">', '</span></footer>', $es_post->ID ); ?>
				</article>
			<?php endif; ?>

			<?php echo es_get_posts( "Termine", "es_print_start_termine" ) ?>
	</div>
</div>

<div class="es-main col-sm-6">
	<div class="es-aktuelles">
		<h1>Aktuelles</h1>
		<?php echo es_get_posts( "Aktuelles", "es_print_start_aktuelles" ) ?>
		<?php if ( get_theme_mod( 'es_info_folder_file' ) ) { ?>
			<div class="es-info-folder panel-body">
				<h2><a href="<?php echo esc_url( get_theme_mod( 'es_info_folder_file' ) ); ?>" alt="<?php echo get_theme_mod( 'es_info_folder_text' ); ?>"><?php echo get_theme_mod( 'es_info_folder_text' ); ?></a></h2>
			</div>
		<?php } ?>
	</div>
</div>

<?php get_footer(); ?>
