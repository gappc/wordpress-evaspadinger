<?php

function set_default_post_order( $post_id ) {
  $category = get_the_category( $post_id );
  if ( !has_category( array( "Aktuelles", "Termine" ), $post_id ) ) {
    return;
  }
	if ( get_post_sort_order( $post_id ) ) {
		return;
  }
	update_post_meta( $post_id, 'es-sortorder', 999);
}
add_action( 'save_post', 'set_default_post_order' );

add_action( 'wp_ajax_es_post_sort_action', 'es_post_sort_callback' );
function es_post_sort_callback() {
  check_ajax_referer( "evaspadinger" );
  if (isset($_POST['sortorder'])) {
    $input = stripslashes($_POST['sortorder']);
    $sortorder = json_decode( $input );
    $count = 0;
    foreach ($sortorder as $order) {
      update_post_meta( $order->id, 'es-sortorder', $count );
      update_post_meta( $order->id, 'es-showpost', $order->show );
      $count++;
    }
    echo $input;
  }
  else {
    echo "Konnte Änderung nicht übernehmen. Bitte kontaktieren Sie den technischen Support";
  }

	wp_die();
}

add_action( 'wp_enqueue_scripts', 'theme_enqueue_styles' );
function theme_enqueue_styles() {
    wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );
}

add_action('admin_init', 'es_hide_title');
function es_hide_title() {
  remove_post_type_support('post', 'editor');
}

function es_get_posts( $category_name, $es_callback = NULL ) {
  if ( is_callable( $es_callback ) ) {
    $es_args = array(
    	'posts_per_page'   => 10,
    	'offset'           => 0,
    	'category'         => '',
    	'category_name'    => $category_name,
    	'orderby'          => 'meta_value_num',
    	'order'            => 'ASC',
    	'include'          => '',
    	'exclude'          => '',
    	'meta_key'         => 'es-sortorder',
    	'meta_value'       => '',
    	'post_type'        => 'post',
    	'post_mime_type'   => '',
    	'post_parent'      => '',
    	'author'	   => '',
    	'post_status'      => 'publish',
    	'suppress_filters' => true
    );
  	$es_posts_array = get_posts( $es_args );

  	foreach ($es_posts_array as $es_post) {
      if (is_show_post($es_post)) {
        call_user_func( $es_callback, $es_post );
      }
    }

  }
}

function es_get_extra_class( $es_post ) {
  $post_categories = wp_get_post_categories( $es_post->ID );
  $categories = [];
  foreach ($post_categories as $c) {
    $cat = get_category($c);
    array_push( $categories, $cat->cat_name );
  }
  $extra_class = "";
  if ( in_array( "Aktuelles", $categories ) ) {
    $extra_class = "es-panel-aktuelles";
  }
  elseif ( in_array( "Termine", $categories ) ) {
    $extra_class = "es-panel-termine";
  }
  return $extra_class;
}

function es_get_inhalt( $es_post, $trim_words_length = 25 ) {
  $inhalt = get_field( 'inhalt', $es_post->ID );
  if ( $inhalt and $trim_words_length > 0 ) {
    return wp_trim_words( $inhalt, $trim_words_length, "..." );
  }
  return $inhalt;
}

function es_print_all_aktuelles( $es_post ) { ?>
  <div class="es-aktuelles es-all">
    <article>
      <h1><?php echo $es_post->post_title ?></h1>
      <div class="panel panel-default panel-body">
        <div class="col-md-3">
          <?php $image = get_field( 'bild', $es_post->ID ); ?>
          <?php if($image) { ?>
            <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
          <?php } ?>
        </div>
        <div class="col-md-9">
          <?php $permalink = get_permalink($es_post->ID); ?>
          <header>
            <h2><?php the_field( 'teaser', $es_post->ID ) ?></h2>
          </header>
          <?php
          $pdf_link = get_field( 'pdf_link', $es_post->ID )['url'];
          $pdf_text = get_field( 'pdf_text', $es_post->ID );
          if (isset($pdf_link) && isset($pdf_text)) {
          ?>
          <div class="es-post-link"><a href="<?= $pdf_link ?>"><?= $pdf_text ?></a></div>
          <?php } ?>
          <?php echo es_get_inhalt( $es_post, -1 ) ?>
          <h6><?php echo get_termin_data( $es_post->ID ) ?></h6>
          <a href="<?php echo $permalink ?>">Weiter...</a>
        </div>
        <?php edit_post_link( __( 'Edit', '_tk' ), '<footer class="entry-meta"><span class="edit-link">', '</span></footer>', $es_post->ID ); ?>
      </div>
    </article>
  </div>
  <?php
}

function es_print_all_termine( $es_post ) { ?>
  <div class="es-termine es-all">
    <article>
      <h1><?php echo $es_post->post_title ?></h1>
      <div class="panel panel-default panel-body">
        <div class="col-md-3">
          <?php $image = get_field( 'bild', $es_post->ID ); ?>
          <?php if($image) { ?>
            <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
          <?php } ?>
        </div>
        <div class="col-md-9">
          <?php $permalink = get_permalink( $es_post->ID ); ?>
          <header>
            <h2><?php the_field( 'teaser', $es_post->ID ) ?></h2>
          </header>
          <?php
          $pdf_link = get_field( 'pdf_link', $es_post->ID )['url'];
          $pdf_text = get_field( 'pdf_text', $es_post->ID );
          if (isset($pdf_link) && isset($pdf_text)) {
          ?>
          <div class="es-post-link"><a href="<?= $pdf_link ?>"><?= $pdf_text ?></a></div>
          <?php } ?>
          <?php echo es_get_inhalt( $es_post, -1 ) ?>
          <h6><?php echo get_termin_data( $es_post->ID ) ?></h6>
          <a href="<?php echo $permalink ?>">Weiter...</a>
        </div>
      <?php edit_post_link( __( 'Edit', '_tk' ), '<footer class="entry-meta"><span class="edit-link">', '</span></footer>', $es_post->ID ); ?>
    </article>
  </div>
  <?php
}

function es_print_start_aktuelles( $es_post ) { ?>
  <article class="panel panel-default panel-body">
    <?php $permalink = get_permalink($es_post->ID); ?>
    <header>
      <h2><a href="<?php echo $permalink ?>"><?php echo $es_post->post_title ?></a></h2>
      <h5><?php echo the_field( 'teaser', $es_post->ID ) ?></h5>
    </header>
    <?php
    $pdf_link = get_field( 'pdf_link', $es_post->ID )['url'];
    $pdf_text = get_field( 'pdf_text', $es_post->ID );
    if (isset($pdf_link) && isset($pdf_text)) {
    ?>
    <div class="es-post-link"><a href="<?= $pdf_link ?>"><?= $pdf_text ?></a></div>
    <?php } ?>
    <?php edit_post_link( __( 'Edit', '_tk' ), '<footer class="entry-meta"><span class="edit-link">', '</span></footer>', $es_post->ID ); ?>
  </article>
  <?php
}

function es_print_start_termine( $es_post ) { ?>
  <article class="panel panel-default panel-body">
    <?php $permalink = get_permalink($es_post->ID); ?>
    <header>
      <h2><a href="<?php echo $permalink ?>"><?php echo $es_post->post_title ?></a></h2>
      <h5><?php echo the_field( 'teaser', $es_post->ID ) ?></h5>
    </header>
    <?php
    $pdf_link = get_field( 'pdf_link', $es_post->ID )['url'];
    $pdf_text = get_field( 'pdf_text', $es_post->ID );
    if (isset($pdf_link) && isset($pdf_text)) {
    ?>
    <div class="es-post-link"><a href="<?= $pdf_link ?>"><?= $pdf_text ?></a></div>
    <?php } ?>
    <h6><?php echo get_termin_data( $es_post->ID ) ?></h6>
    <?php edit_post_link( __( 'Edit', '_tk' ), '<footer class="entry-meta"><span class="edit-link">', '</span></footer>', $es_post->ID ); ?>
  </article>
  <?php
}

function get_contact() {
  $email = get_theme_mod( 'es_contact_email' );
  return ""
    . "<div>" . get_theme_mod( 'es_contact_street' ) . "</div>"
    . "<div>" . get_theme_mod( 'es_contact_plz' ) . "</div>"
    . "<div>" . get_theme_mod( 'es_contact_phone' ) . "</div>"
    . "<div><a title='Email Adresse' href='mailto:" . $email . "'>" . $email . "</a></div>";
}

function print_contact() { ?>
  <div><?php echo get_theme_mod( 'es_contact_street' ) ?></div>
  <div><?php echo get_theme_mod( 'es_contact_plz' ) ?></div>
  <div><?php echo get_theme_mod( 'es_contact_phone' ) ?></div>
  <?php $es_email = get_theme_mod( 'es_contact_email' ); ?>
  <div><a title="Email Adresse" href="mailto:<?php echo $es_email ?>"><?php echo $es_email ?></a></div>
  <?php
}

function get_systemaufstellungen_gruppe_termine() {
  $result = "";
  $raw_dates = get_theme_mod( 'es_termine_datepicker' );
  if ( $raw_dates and strlen( $raw_dates ) > 0 ) {
    $dates = explode( ",", $raw_dates);
    foreach ($dates as $date) {
      if (time() - strtotime($date) < 0) {
        $result = $result . "<span class='es-termin-future'>" . $date . "</span> ";
      }
      else {
        $result = $result . "<span class='es-termin-past'>" . $date . "</span> ";
      }
    }
  }
  return $result;
}

function get_post_sort_order( $post_id ) {
  return get_post_meta( $post_id, 'es-sortorder', true );
}

function is_show_post( $post ) {
  return get_post_meta( $post->ID, 'es-showpost', true );
}

// function get_termin_data( $post_id ) {
//   $datum = get_field( 'datum', $post_id );
//   $uhrzeit = get_field( 'uhrzeit', $post_id );
//   $ort = get_field( 'ort', $post_id );
//   $result = "";
//   if ( $datum ) {
//     $result = $datum;
//   }
//   if ( $uhrzeit ) {
//     if ( strlen( $result ) > 0) {
//       $result = $result . ", ";
//     }
//     $result = $result . $uhrzeit;
//   }
//   if ( $ort ) {
//     if ( strlen( $result ) > 0) {
//       $result = $result . ", ";
//     }
//     $result = $result . $ort;
//   }
//   return $result;
// }

// function get_termin_data( $post_id ) {
//   $datum = get_field( 'datum', $post_id );
//   $uhrzeit = get_field( 'uhrzeit', $post_id );
//   $ort = get_field( 'ort', $post_id );
//   $result = "";
//   if ( $datum ) {
//     $result = "Datum: " . $datum;
//   }
//   if ( $uhrzeit ) {
//     if ( strlen( $result ) > 0) {
//       $result = $result . "</br>";
//     }
//     $result = $result . "Uhrzeit: " . $uhrzeit;
//   }
//   if ( $ort ) {
//     if ( strlen( $result ) > 0) {
//       $result = $result . "</br>";
//     }
//     $result = $result . "Ort: " . $ort;
//   }
//   return $result;
// }

function get_termin_data( $post_id ) {
  $datum = get_field( 'datum', $post_id );
  $uhrzeit = get_field( 'uhrzeit', $post_id );
  $ort = get_field( 'ort', $post_id );
  $result = "<table class='termin-data'>";
  if ( $datum ) {
    $result = $result . "<tr><td>Datum:</td><td>" . $datum . "</td></tr>";
  }
  if ( $uhrzeit ) {
    $result = $result . "<tr><td>Uhrzeit:</td><td>" . $uhrzeit . "</td></tr>";
  }
  if ( $ort ) {
    $result = $result . "<tr><td>Ort:</td><td>" . $ort . "</td></tr>";
  }
  $result = $result . "</table>";
  return $result;
}

require 'inc/customizer.php';
require 'inc/shortcodes.php';
