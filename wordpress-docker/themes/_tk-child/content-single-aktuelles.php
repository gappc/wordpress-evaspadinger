<?php
/**
 * @package _tk
 */
?>
<?php $ort = get_field( 'ort' ); ?>
<?php $gmaps = get_field( 'google_maps' ); ?>

<div class="es-aktuelles es-all">
	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		<header>
			<h1 class="page-title"><?php the_title(); ?></h1>
			<?php $image = get_field( 'bild', get_the_ID() ); ?>
			<?php if($image) { ?>
				<img class="single-header-image" src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
			<?php } ?>
			<h2 class="single-teaser"><?php the_field( 'teaser', get_the_ID() ); ?></h2>
		</header><!-- .entry-header -->

		<?php
		$pdf_link = get_field( 'pdf_link', $es_post->ID )['url'];
		$pdf_text = get_field( 'pdf_text', $es_post->ID );
		if (isset($pdf_link) && isset($pdf_text)) {
		?>
		<div class="single-article-body"><a href="<?= $pdf_link ?>"><?= $pdf_text ?></a></div>
		<?php } ?>

		<div class="entry-content single-article-body">
			<?php the_field( 'inhalt', get_the_ID() ); ?>
			<h6><?php echo get_termin_data( get_the_ID() ) ?></h6>
			<?php edit_post_link( __( 'Edit', '_tk' ), '<footer class="entry-meta"><span class="edit-link">', '</span></footer>', get_the_ID() ); ?>
		</div><!-- .entry-content -->

		<div id="map"></div>

	</article><!-- #post-## -->

	<?php if ($gmaps) : ?>
		<style>
		  #map {
				margin-top: 30px;
		    height: 400px;
		  }
			#map img {
				max-width:none !important;
			}
		</style>
		<script src="https://maps.googleapis.com/maps/api/js"></script>
		<script>
		  function initialize() {
				var mapCanvas = document.getElementById('map');
				var mapOptions = {
		      center: new google.maps.LatLng(<?php echo $gmaps["lat"]; ?>, <?php echo $gmaps["lng"]; ?>),
		      zoom: 14,
		      mapTypeId: google.maps.MapTypeId.ROADMAP
		    }
		    var map = new google.maps.Map(mapCanvas, mapOptions);

				var marker = new google.maps.Marker({
					position: map.getCenter(),
					map: map
				});

				var infowindow = new google.maps.InfoWindow({
					content: "<h5><?php the_title(); ?></h5><?php echo get_termin_data( get_the_ID() ); ?>"
				});

				google.maps.event.addListener(marker, 'click', function() {
					infowindow.open(map, marker);
				});
			}

			google.maps.event.addDomListener(window, 'load', initialize);
		</script>
	<?php endif; ?>
	</div>
