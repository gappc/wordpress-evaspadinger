<?php
/**
 * The Template for displaying all single posts.
 *
 * @package _tk
 */

get_header(); ?>

	<?php while ( have_posts() ) : the_post(); ?>

		<?php if ( has_category( "Aktuelles" ) ) {
			get_template_part( 'content', 'single-aktuelles' );
		}
		else if ( has_category( "Termine" ) ) {
			get_template_part( 'content', 'single-termine' );
		}
		else {
			get_template_part( 'content', 'single' );
		} ?>

	<?php endwhile; // end of the loop. ?>

<?php get_footer(); ?>
