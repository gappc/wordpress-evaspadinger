<?php
/*
 * Template Name: Kontakt Seite
 */

get_header(); ?>

<div class="col-sm-12">
	<h3>Eva Spadinger</h3>
	<?php print_contact(); ?>
	<div><a title="Homepage Adresse" href="http://www.ebenenwechsel.com">http://www.ebenenwechsel.com</a></div>
	<?php while ( have_posts() ) : the_post(); ?>
		<?php the_content(); ?>
	<?php endwhile ?>
</div>

<?php
	$lat = get_theme_mod( 'es_contact_gmaps_lat' );
	$lng = get_theme_mod( 'es_contact_gmaps_lng' );

	if (isset($lat) && isset($lng)) : ?>
	<div class="col-sm-12">
		<div id="map"></div>
	</div>
	<style>
		#map {
			height: 400px;
			width: 100%;
		}
		#map img {
			max-width:none !important;
		}
	</style>
	<script src="https://maps.googleapis.com/maps/api/js"></script>
	<script>
		function initialize() {
			var mapCanvas = document.getElementById('map');
			var mapOptions = {
				center: new google.maps.LatLng(<?php echo $lat; ?>, <?php echo $lng; ?>),
				zoom: 14,
				mapTypeId: google.maps.MapTypeId.ROADMAP
			}
			var map = new google.maps.Map(mapCanvas, mapOptions);

			var marker = new google.maps.Marker({
				position: map.getCenter(),
				map: map
			});

			var infowindow = new google.maps.InfoWindow({
				content: "<?php echo get_contact() ?>"
			});

			google.maps.event.addListener(marker, 'click', function() {
				infowindow.open(map, marker);
			});
		}

		google.maps.event.addDomListener(window, 'load', initialize);
	</script>
<?php endif; ?>

<?php get_footer(); ?>
